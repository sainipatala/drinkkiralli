'use strict';

var utils = require('../utils/writer.js');
var Drink = require('../service/DrinkService');

module.exports.addDrink = function addDrink (req, res, next) {
  let drink = req.swagger.params['drink'].value;
  Drink.addDrink(drink)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getDrinkById = function getDrinkById (req, res, next) {
  var drinkId = req.swagger.params['drinkId'].value;
  Drink.getDrinkById(drinkId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getDrinks = function getDrinks (req, res, next) {
  Drink.getDrinks()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
