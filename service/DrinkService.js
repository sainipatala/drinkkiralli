'use strict';
const bodyParser = require('body-parser');
let {mongoose} = require('./../db/mongoose');
let Drink = require('./../models/drink');


/**
 * Add a new drink
 *
 * drink Drink Drink object that needs to be added
 * no response value expected for this operation
 **/
exports.addDrink = function(drink) {
  let newdrink = new Drink(drink)
  return new Promise(function(resolve, reject) {
    newdrink.save((err, docs) => {
      if (err) {
        console.log(err)
        reject()
      } else {
        resolve(docs)
      }
    })
  });
}


/**
 * Find drink by ID
 * Returns a single drink
 *
 * drinkId Long ID of drink to return
 * returns Drink
 **/
exports.getDrinkById = function(drinkId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "incredients" : [ {
    "amount" : 6.02745618307040320615897144307382404804229736328125,
    "name" : "name"
  }, {
    "amount" : 6.02745618307040320615897144307382404804229736328125,
    "name" : "name"
  } ],
  "name" : "VilleVallaton",
  "id" : 0,
  "isActive" : true
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Find all drinks
 * Returns all drinks
 *
 * returns List
 **/
exports.getDrinks = function() {
  return new Promise(function(resolve, reject) {
    Drink.find({}, function (err, docs) {
      if (err) {
        console.log(err)
        reject()
      } else {
        resolve(docs);
      }
    });
  });
}
