const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DrinkSchema = new Schema({
  name: {type: String, required: true},
  isActive: {type: Boolean, required: true},
  incredients: [{
    name: {type: String, required: true},
    amount: {type: Number, required: true}, 
  }]
})

module.exports = mongoose.model('Drink', DrinkSchema);
